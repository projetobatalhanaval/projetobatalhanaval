#include <stdio.h>
#include <stdlib.h>
#define PRETO 0
#define AZUL 1
#define VERDE 2
#define VERDEA 3
#define VERMELHO 4
#define ROXO 5
#define AMARELO 6
#define BRANCO 7
#define NEGRITO 8
#define AZULC 9
#define VERDEC 10//A
#define AZULCC 11//B
#define VERMELHOC 12//C
#define LILAS 13//D
#define AMARELOC 14//E
#define BRANCOB 15//F

char mascara1 [14][14], mascara2 [14][14], arena1 [14][14], arena2 [14][14], nome_jogador1 [50], nome_jogador2 [50], letracol, sentido;
int i, j, lin, col, inicia_jogo;

void Creditos () // Mostra os cr�ditos de desenvolvimento
{
	system ("color 17");
	system ("cls");
	printf ("\n\n\n");
	printf ("              ");
	for (i=0;i<51;i++)
	printf ("=");
		printf ("\n              |                                                 |");
		printf ("\n              |                DESENVOLVEDORES:                 |");
		printf ("\n              |               ANDERSON FERNANDES                |");
		printf ("\n              |                  CAIO MARTINS                   |");
		printf ("\n              |                 GUSTAVO FRANCO                  |");
		printf ("\n              |                  LUCAS CASTRO                   |");
		printf ("\n              |                                                 |");
		printf ("\n              |           PROFESSORA: MILENE ARANTES            |");
		printf ("\n              |      2%c SEMESTRE - ENGENHARIA DE SOFTWARE       |", 167);
		printf ("\n              |               UNIVERSIDADE UNIFAE               |");
		printf ("\n              |                                                 |");
		printf ("\n");
	printf ("              ");
	for (i=0;i<51;i++)
		printf ("=");
	printf ("\n\n\n");
	exit (0);
}

void erro_letra () // Erro caso seja informada uma letra inv�lida
{
	system ("cls");
	printf ("\n\n\n\n\n\n\n\n\n");
	printf ("\n              ===================================================");
	printf ("\n              |               COORDENADA INV%cLIDA               |", 181);
	printf ("\n              |                 LETRA N%CO EXISTE                |", 199);
	printf ("\n              |        POR FAVOR, INFORME OUTRA COORDENADA      |");
	printf ("\n              ===================================================");
	printf ("\n\n\n\t\t   ");
	system ("pause");
	system ("cls");
}

void erro_numero () // Erro caso seja informado um numero inv�lido
{
	system ("cls");
	printf ("\n\n\n\n\n\n\n\n\n");
	printf ("\n              ===================================================");
	printf ("\n              |               COORDENADA INV%cLIDA               |", 181);
	printf ("\n              |                N%cMERO N%CO EXISTE                |", 233, 199);
	printf ("\n              |        POR FAVOR, INFORME OUTRA COORDENADA      |");
	printf ("\n              ===================================================");
	printf ("\n\n\n\t\t   ");
	system ("pause");
	system ("cls");
}

void erro_sentido () // Erro caso seja informado um sentido inv�lido
{
	system ("cls");
	printf ("\n\n\n\n\n\n\n\n\n");
	printf ("\n              ===================================================");
	printf ("\n              |                 SENTIDO INV%cLIDO                |", 181);
	printf ("\n              |       POR FAVOR, INFORME UM SENTIDO V%cLIDO      |", 181);
	printf ("\n              ===================================================");
	printf ("\n\n\n\t\t   ");
	system ("pause");
	system ("cls");
}

void erro () // Erro caso haja sobreposi��o entre navios
{
	system ("cls");
	printf ("\n\n\n\n\n\n\n\n\n");
	printf ("\n              ===================================================");
	printf ("\n              |               COORDENADA INV%cLIDA               |", 181);
	printf ("\n              |                ESPA%cO J%c OCUPADO                |", 128, 181);
	printf ("\n              |       POR FAVOR, INFORME OUTRA COORDENADA       |");
	printf ("\n              ===================================================");
	printf ("\n\n\n\t\t   ");
	system ("pause");
	system ("cls");
}

void erro_tabuleiro () // Erro caso os limites do tabuleiro sejam ultrapassados
{
	system ("cls");
	printf ("\n\n\n\n\n\n\n\n\n");
	printf ("\n              ===================================================");
	printf ("\n              |                      ERRO!                      |");
	printf ("\n              |         LIMITES DO TABULEIRO ULTRAPASSADO       |");
	printf ("\n              |        POR FAVOR, INFORME OUTRA COORDENADA      |");
	printf ("\n              ===================================================");
	printf ("\n\n\n\t\t   ");
	system ("pause");
	system ("cls");
}

void erro_opcao () // Erro caso seja informada uma op��o inv�lida
{
	system ("cls");
	printf ("\n\n\n\n\n\n\n\n");
	printf ("\n              ===================================================");
	printf ("\n              |                       ERRO!                     |");
	printf ("\n              |                 OP%c%cO N%cO EXISTE                |", 128, 199, 199);
	printf ("\n              |        POR FAVOR, INFORME UMA OP%c%cO V%cLIDA      |", 128, 199, 181);
	printf ("\n              ===================================================");
	printf ("\n\n\n\t\t   ");
	system ("pause");
	system ("cls");
}

void informa_nomes () // Informa os nomes dos 2 jogadores
{
	int opcao2;
		
	opcao2=2;
	while (opcao2==2)
	{
		system ("cls");
		printf ("\n              ===================================================");
		printf ("\n              |           A BATALHA ESTA PARA COME%cAR           |", 128);
		printf ("\n              ===================================================");
		printf ("\n\n\n");
		printf ("\n              ===================================================");
		printf ("\n              |Informe um nome para o JOGADOR 1: ");
		scanf ("%s", nome_jogador1);
		printf ("              |Informe um nome para o JOGADOR 2: ");
		scanf ("%s", nome_jogador2);
		printf ("              ===================================================");
		printf ("\n\n\n");
		printf ("              ===================================================\n");
		printf ("              |                    CONFIRMAR                    |\n");
		printf ("              |                     1 - SIM                     |\n");
		printf ("              |                     2 - N%cO                     |\n", 199);
		printf ("              ===================================================\n");
		printf ("\n");
		printf ("                             INFORME A SUA OP%c%cO: ", 128, 199);
		scanf ("%d", &opcao2);
		if (opcao2 != 1 && opcao2 != 2)
		{
			erro_opcao ();
			informa_nomes ();
		}
	}
}

void legenda1 () // Legenda para aloca��o dos navios
{
	printf ("\n");
	printf ("                	  =========================\n");
	printf ("                	  |        LEGENDA:       |\n");
	printf ("                	  |      '~' - %cgua       |\n", 181);
	printf ("                	  |'O' - Parte de um navio|\n");
	printf ("                	  =========================\n\n");
}

void legenda2 () // Legenda para a fase de tiros
{
	printf ("\n");
	printf ("\t\t   =====================================\n");
	printf ("\t\t   |             LEGENDA:              |\n");
	printf ("\t\t   |            '~' - %cgua             |\n", 181);
	printf ("\t\t   |         '*' - Espa%co vazio        |\n", 135);
	printf ("\t\t   | 'X' - Parte de um navio destruido |\n");
	printf ("\t\t   =====================================\n\n");
}

void inicia_arena () // Fun�ao para mostrar e definir valores para as arenas
{
	char letras [44] = {0,0,'A',0,0,'B',0,0,'C',0,0,'D',0,0,'E',0,0,'F',0,0,'G',0,0,'H',0,0,'I',0,0,'J',0,0,'K',0,0,'L',0,0,'M',0,0,'N',0,0};
	char borda [44] = {201,205,205,205,205,205,205,205,205,205,205,205,205,205,205,205,205,205,205,205,205,205,205,205,205,205,205,205,205,205,205,205,205,205,205,205,205,205,205,205,205,205,205,187};
	char borda2 [44] = {200,205,205,205,205,205,205,205,205,205,205,205,205,205,205,205,205,205,205,205,205,205,205,205,205,205,205,205,205,205,205,205,205,205,205,205,205,205,205,205,205,205,205,188};

	system ("cls");
	system ("color 0B");
	printf ("===============================================================================");
	printf ("\n                	  VAMOS L%c %s!                                    ", 181, nome_jogador1);
	printf ("\n                	  HORA DE CONSTRUIR SUA ARMADA ");
	printf ("\n===============================================================================");
	legenda1 ();

	printf ("\t\t"); // Tabula��o	das letras
	for (i=0; i<44; i++) // Impress�o das letras
		printf ("%c", letras[i]);

	printf ("\n\t\t"); // Tabula��o da parte superior da borda
	for (i=0; i<44; i++) // Imprimi a borda horizontal superior
		printf ("%c", borda[i]);

	printf ("\n"); // Pula uma linha
	for (i=0; i<14; i++)
	{
		printf ("\t%d\t", i+1); // Impress�o dos n�meros na direita
		printf ("%c", 186); // Impress�o do come�o da borda vertical

		for (j=0; j<14; j++)
		{
			arena1 [i][j] ='~'; // Representa��o �gua
			mascara1 [i][j] ='~'; // Representa��o �gua
			arena2 [i][j] ='~'; // Representa��o �gua
			mascara2 [i][j] ='~'; // Representa��o �gua
			printf (" %c ", arena1[i][j]); // Impress�o da representa��o da �gua
		}

	printf ("%c", 186); // Impress�o no final a borda vertical
	printf ("\n");
	}

	printf ("\t\t"); // Tabula��o da parte inferior da borda
	for (i=0; i<44; i++)
		printf ("%c", borda2[i]); // Impress�o no final da tabela a borda horizontal
}

void mostrar_arena1 () // Mostra arena do Jogador 1
{
	char letras [44] = {0,0,'A',0,0,'B',0,0,'C',0,0,'D',0,0,'E',0,0,'F',0,0,'G',0,0,'H',0,0,'I',0,0,'J',0,0,'K',0,0,'L',0,0,'M',0,0,'N',0,0};
	char borda [44] = {201,205,205,205,205,205,205,205,205,205,205,205,205,205,205,205,205,205,205,205,205,205,205,205,205,205,205,205,205,205,205,205,205,205,205,205,205,205,205,205,205,205,205,187};
	char borda2 [44] = {200,205,205,205,205,205,205,205,205,205,205,205,205,205,205,205,205,205,205,205,205,205,205,205,205,205,205,205,205,205,205,205,205,205,205,205,205,205,205,205,205,205,205,188};

	system ("cls");
	printf ("===============================================================================");
	printf ("\n                	  VAMOS L%c %s!                                    ", 181, nome_jogador1);
	printf ("\n                	  HORA DE CONSTRUIR SUA ARMADA ");
	printf ("\n===============================================================================");
	legenda1 ();

	printf ("\t\t"); // Tabula��o	das letras
	for (i=0; i<44; i++) // Impress�o das letras
		printf ("%c", letras[i]);

	printf ("\n\t\t"); // Tabula��o da parte superior da borda
	for (i=0; i<44; i++) // Imprimi a borda horizontal superior
		printf ("%c", borda[i]);

	printf ("\n"); // Pula uma linha
	for (i=0; i<14; i++)
	{
		printf ("\t%d\t", i+1); // Impress�o dos n�meros na direita
		printf ("%c", 186); // Impress�o do come�o da borda vertical

		for (j=0; j<14; j++)
			printf (" %c ", arena1[i][j]); // Impress�o do tabuleiro

	printf ("%c", 186); // Impress�o no final a borda vertical
	printf ("\n");
	}

	printf ("\t\t"); // Tabula��o da parte inferior da borda
	for (i=0; i<44; i++)
		printf ("%c", borda2[i]); // Impress�o no final da tabela a borda horizontal
}

void mostrar_arena2 () // Mostra arena do Jogador 2
{
	char letras [44] = {0,0,'A',0,0,'B',0,0,'C',0,0,'D',0,0,'E',0,0,'F',0,0,'G',0,0,'H',0,0,'I',0,0,'J',0,0,'K',0,0,'L',0,0,'M',0,0,'N',0,0};
	char borda [44] = {201,205,205,205,205,205,205,205,205,205,205,205,205,205,205,205,205,205,205,205,205,205,205,205,205,205,205,205,205,205,205,205,205,205,205,205,205,205,205,205,205,205,205,187};
	char borda2 [44] = {200,205,205,205,205,205,205,205,205,205,205,205,205,205,205,205,205,205,205,205,205,205,205,205,205,205,205,205,205,205,205,205,205,205,205,205,205,205,205,205,205,205,205,188};

	system ("cls");
	printf ("===============================================================================");
	printf ("\n                	  VAMOS L%c %s!                                    ", 181, nome_jogador2);
	printf ("\n                	  HORA DE CONSTRUIR SUA ARMADA ");
	printf ("\n===============================================================================");
	printf ("\n");
	legenda1 ();

	printf ("\t\t"); // Tabula��o	das letras

	for (i=0; i<44; i++) // Impress�o das letras
		printf ("%c", letras[i]);

	printf ("\n\t\t"); // Tabula��o da parte superior da borda
	for (i=0; i<44; i++) // Imprimi a borda horizontal superior
		printf ("%c", borda[i]);

	printf ("\n"); // Pula uma linha
	for (i=0; i<14; i++)
	{
		printf ("\t%d\t", i+1); // Impress�o dos n�meros na direita
		printf ("%c", 186); // Impress�o do come�o da borda vertical

		for (j=0; j<14; j++)
			printf (" %c ", arena2[i][j]); // Impress�o do tabuleiro
			
	printf ("%c", 186); // Impress�o no final a borda vertical
	printf ("\n");

	}

	printf ("\t\t"); // Tabula��o da parte inferior da borda
	for (i=0; i<44; i++)
		printf ("%c", borda2[i]); // Impress�o no final da tabela a borda horizontal
}

void mostrar_mascara1 () // Mostra arena do Jogador 2 sem os navios
{
	char letras [44] = {0,0,'A',0,0,'B',0,0,'C',0,0,'D',0,0,'E',0,0,'F',0,0,'G',0,0,'H',0,0,'I',0,0,'J',0,0,'K',0,0,'L',0,0,'M',0,0,'N',0,0};
	char borda [44] = {201,205,205,205,205,205,205,205,205,205,205,205,205,205,205,205,205,205,205,205,205,205,205,205,205,205,205,205,205,205,205,205,205,205,205,205,205,205,205,205,205,205,205,187};
	char borda2 [44] = {200,205,205,205,205,205,205,205,205,205,205,205,205,205,205,205,205,205,205,205,205,205,205,205,205,205,205,205,205,205,205,205,205,205,205,205,205,205,205,205,205,205,205,188};

	system ("cls");
	printf ("===============================================================================");
	printf ("\n                	  %s!                                    ", nome_jogador2);
	printf ("\n                	  %c A SUA VEZ DE JOGAR! ", 144);
	printf ("\n===============================================================================");
	legenda2 ();

	printf ("\t\t"); // Tabula��o das letras
	for (i=0; i<44; i++) // Impress�o das letras
		printf ("%c", letras[i]);

	printf ("\n\t\t"); // Tabula��o da parte superior da borda
	for (i=0; i<44; i++) // Imprimi a borda horizontal superior
		printf ("%c", borda[i]);

	printf ("\n"); // Pula uma linha
	for (i=0; i<14; i++)
	{
		printf ("\t%d\t", i+1); // Impress�o dos n�meros na direita
		printf ("%c", 186); // Impress�o do come�o da borda vertical

		for (j=0; j<14; j++)
			printf (" %c ", mascara1[i][j]); // Impress�o do tabuleiro

		printf ("%c", 186); // Impress�o no final a borda vertical
		printf ("\n");
	}

	printf ("\t\t"); // Tabula��o da parte inferior da borda
	for (i=0; i<44; i++)
		printf ("%c", borda2[i]); // Impress�o no final da tabela a borda horizontal
}

void mostrar_mascara2 () // Mostra arena do Jogador 2 sem os navios
{
	char letras [44] = {0,0,'A',0,0,'B',0,0,'C',0,0,'D',0,0,'E',0,0,'F',0,0,'G',0,0,'H',0,0,'I',0,0,'J',0,0,'K',0,0,'L',0,0,'M',0,0,'N',0,0};
	char borda [44] = {201,205,205,205,205,205,205,205,205,205,205,205,205,205,205,205,205,205,205,205,205,205,205,205,205,205,205,205,205,205,205,205,205,205,205,205,205,205,205,205,205,205,205,187};
	char borda2 [44] = {200,205,205,205,205,205,205,205,205,205,205,205,205,205,205,205,205,205,205,205,205,205,205,205,205,205,205,205,205,205,205,205,205,205,205,205,205,205,205,205,205,205,205,188};

	system ("cls");
	printf ("===============================================================================");
	printf ("\n                	  %s!                                    ", nome_jogador1);
	printf ("\n                	  %c A SUA VEZ DE JOGAR! ", 144);
	printf ("\n===============================================================================");
	legenda2 ();

	printf ("\t\t"); // Tabula��o das letras
	for (i=0; i<44; i++) // Impress�o das letras
		printf ("%c", letras[i]);

	printf ("\n\t\t"); // Tabula��o da parte superior da borda
	for (i=0; i<44; i++) // Imprimi a borda horizontal superior
		printf ("%c", borda[i]);

	printf ("\n"); // Pula uma linha
	for (i=0; i<14; i++)
	{
		printf ("\t%d\t", i+1); // Impress�o dos n�meros na direita
		printf ("%c", 186); // Impress�o do come�o da borda vertical

		for (j=0; j<14; j++)
			printf (" %c ", mascara2[i][j]); // Impress�o do tabuleiro

		printf ("%c", 186); // Impress�o no final a borda vertical
		printf ("\n");
	}

	printf ("\t\t"); // Tabula��o da parte inferior da borda
	for (i=0; i<44; i++)
		printf ("%c", borda2[i]); // Impress�o no final da tabela a borda horizontal
}

void letra_numero () // Faz a convers�o de letra para numero
{
	if (letracol=='a' || letracol=='A')
		col=1;

	if (letracol=='b' || letracol=='B')
  		col=2;

  	if (letracol=='c' || letracol=='C')
  		col=3;

  	if (letracol=='d' || letracol=='D')
  		col=4;

    if (letracol=='e' || letracol=='E')
  		col=5;

    if (letracol=='f' || letracol=='F')
  		col=6;

    if (letracol=='g' || letracol=='G')
  		col=7;

    if (letracol=='h' || letracol=='H')
  		col=8;

    if (letracol=='i' || letracol=='I')
  		col=9;

    if (letracol=='j' || letracol=='J')
  		col=10;

    if (letracol=='k' || letracol=='K')
  		col=11;

    if (letracol=='l' || letracol=='L')
  		col=12;

    if (letracol=='m' || letracol=='M')
  		col=13;

    if (letracol=='n' || letracol=='N')
  		col=14;

}

void alocar_corveta1 () // Faz a aloca��o da corveta para o Jogador 1
{
	printf ("\n\n\t\tInforme a coordenada do Corveta:");
	printf ("\n\t\tLetra: ");
	scanf ("%s", &letracol);

	while (letracol != 'A' && letracol != 'B' && letracol != 'C' && letracol != 'D' && letracol != 'E' && letracol != 'F' && letracol != 'G' && letracol != 'H' && letracol != 'I' && letracol != 'J' && letracol != 'K' && letracol != 'L' && letracol != 'M' && letracol != 'N'  && letracol != 'a' && letracol != 'b' && letracol != 'c'&& letracol != 'd' && letracol != 'e' && letracol != 'f' && letracol != 'g' && letracol != 'h' && letracol != 'i' && letracol != 'j' && letracol != 'k' && letracol != 'l' && letracol != 'm' && letracol != 'n')
	{
		erro_letra ();
		mostrar_arena1 ();
		printf("\n\n\t\tInforme a coordenada do Corveta:");
		printf("\n\t\tLetra: ");
		scanf("%s", &letracol);
	}

	printf ("\t\tN%cmero: ", 163);
	scanf ("%d", &lin);

	while (lin < 1 || lin > 14)
	{
		erro_numero ();
		mostrar_arena1();
		printf("\n\n\t\tN%cmero: ", 163);
		scanf("%d", &lin);
	}

	letra_numero ();
	printf ("\n\t\tSentidos dispon%cveis: \n", 213);
	printf ("\t\t[d] - Direita\n");
	printf ("\t\t[e] - Esquerda\n");
	printf ("\t\t[c] - Cima\n");
	printf ("\t\t[b] - Baixo\n");
	printf ("\n");
	printf ("\t\tInforme o sentido desejado: ");
	scanf ("%s", &sentido);
	while (sentido != 'd' && sentido != 'D' && sentido != 'b' && sentido != 'B' && sentido != 'e' && sentido != 'E' && sentido != 'c' && sentido != 'C')
	{
		erro_sentido ();
		mostrar_arena1 ();
		printf ("\n\n\t\tSentidos dispon%cveis: \n", 213);
		printf ("\t\t[d] - Direita\n");
		printf ("\t\t[e] - Esquerda\n");
		printf ("\t\t[c] - Cima\n");
		printf ("\t\t[b] - Baixo\n");
		printf ("\n");
		printf ("\t\tInforme o sentido desejado: ");
		scanf ("%s", &sentido);
	}

	if (sentido == 'd' || sentido == 'D')
	{
		if (arena1[lin-1][col-1]=='O' || arena1[lin-1][col]=='O') // Condi��o para evitar sobrepor os navios
		{
			erro ();
			mostrar_arena1 ();
			alocar_corveta1 (); // Reinicia a fun��o caso der erro
		}
		else
		{
			if (lin - 1 < 0 || lin - 1 > 14 || col - 1 < 0 || col - 1 > 13  || col < 0 || col > 13) // Condi��o para limitar os extremos da matriz
			{
				erro_tabuleiro ();
			    mostrar_arena1 ();
			    alocar_corveta1 (); // Reinicia a fun��o caso der erro
			}
			else
			{
				arena1[lin-1][col-1]='O';
			    arena1[lin-1][col]='O';
			    mostrar_arena1();
			}
		}
	}

	if (sentido == 'b' || sentido == 'B')
	{
		if (arena1[lin-1][col-1]=='O' || arena1[lin][col-1]=='O') // Condi��o para evitar sobrepor os navios
		{
			erro ();
			mostrar_arena1 ();
			alocar_corveta1 (); // Reinicia a fun��o caso der erro
		}
		else
		{
           	if (lin - 1 < 0 || lin-1 > 13 || lin < 0 || lin > 13 || col - 1 < 0 || col - 1 > 14 ) // Condi��o para limitar os extremos da matriz
			{
          		erro_tabuleiro ();
				mostrar_arena1 ();
				alocar_corveta1 (); // Reinicia a fun��o caso der erro
			}
			else
			{
				arena1[lin-1][col-1]='O';
				arena1[lin][col-1]='O';
				mostrar_arena1 ();
			}
		}
	}

	if (sentido == 'e' || sentido == 'E')
	{
		if (arena1[lin-1][col-1]=='O' || arena1[lin-1][col-2]=='O') // Condi��o para evitar sobrepor os navios
		{
			erro ();
			mostrar_arena1 ();
			alocar_corveta1 (); // Reinicia a fun��o caso der erro
		}
		else
		{
			if (lin - 1 < 0 || lin-1 > 14 || col - 1 < 0 || col - 1 > 14  || col - 2 < 0 || col - 2 > 14) // Condi��o para limitar os extremos da matriz
			{
				erro_tabuleiro ();
				mostrar_arena1 ();
			    alocar_corveta1 (); // Reinicia a fun��o caso der erro
			}
			else
			{
				arena1[lin-1][col-1]='O';
				arena1[lin-1][col-2]='O';
				mostrar_arena1 ();
	    	}
		}
   }

	if (sentido == 'c' || sentido == 'C')
	{
		if (arena1[lin-1][col-1]=='O' || arena1[lin-2][col-1]=='O') // Condi��o para evitar sobrepor os navios
		{
			erro ();
			mostrar_arena1 ();
			alocar_corveta1 (); // Reinicia a fun��o caso der erro
		}
		else
		{
			if (col - 1 < 0 || col - 1 > 14  || lin - 1 < 0 || lin-1 > 14 || lin - 2 < 0 || lin - 2 > 14) // Condi��o para limitar os extremos da matriz
			{
				erro_tabuleiro ();
			    mostrar_arena1 ();
			    alocar_corveta1 (); // Reinicia a fun��o caso der erro
			}
			else
			{
				arena1[lin-1][col-1]='O';
			 	arena1[lin-2][col-1]='O';
			 	mostrar_arena1 ();
			}
		}
	}
 }

void alocar_submarino1 () // Faz a aloca��o do submarino para o Jogador 1
{
	char sentido;
	printf ("\n\n\t\tInforme a coordenada do Submarino:");
	printf ("\n\t\tLetra: ");
	scanf ("%s", &letracol);
	while (letracol != 'A' && letracol != 'B' && letracol != 'C'&& letracol != 'D' && letracol != 'E' && letracol != 'F' && letracol != 'G' && letracol != 'H' && letracol != 'I' && letracol != 'J' && letracol != 'K' && letracol != 'L' && letracol != 'M' && letracol != 'N'  && letracol != 'a' && letracol != 'b' && letracol != 'c'&& letracol != 'd' && letracol != 'e' && letracol != 'f' && letracol != 'g' && letracol != 'h' && letracol != 'i' && letracol != 'j' && letracol != 'k' && letracol != 'l' && letracol != 'm' && letracol != 'n')
	{
		erro_letra ();
		mostrar_arena1 ();
		printf ("\n\n\t\tInforme a coordenada do Corveta:");
		printf ("\n\t\tLetra: ");
		scanf ("%s", &letracol);
	}
	printf ("\t\tN%cmero: ", 163);
	scanf ("%d", &lin);
	while (lin < 1 || lin > 14)
	{
		erro_numero ();
		mostrar_arena1 ();
		printf("\n\n\t\tN%cmero: ", 163);
		scanf ("%d", &lin);
	}
	letra_numero ();
	printf ("\n\t\tSentidos dispon%cveis: \n", 213);
	printf ("\t\t[d] - Direita\n");
	printf ("\t\t[e] - Esquerda\n");
	printf ("\t\t[c] - Cima\n");
	printf ("\t\t[b] - Baixo\n");
	printf ("\n");
	printf ("\t\tInforme o sentido desejado: ");
	scanf ("%s", &sentido);
	while (sentido != 'd' && sentido != 'D' && sentido != 'b' && sentido != 'B' && sentido != 'e' && sentido != 'E' && sentido != 'c' && sentido != 'C')
	{
		erro_sentido ();
		mostrar_arena1 ();
		printf ("\n\n\t\tSentidos dispon%cveis: \n", 213);
		printf ("\t\t[d] - Direita\n");
		printf ("\t\t[e] - Esquerda\n");
		printf ("\t\t[c] - Cima\n");
		printf ("\t\t[b] - Baixo\n");
		printf ("\n");
		printf ("\t\tInforme o sentido desejado: ");
		scanf ("%s", &sentido);
	}

	if (sentido == 'd' || sentido == 'D')
	{
		if (arena1[lin-1][col-1]=='O' || arena1[lin-1][col]=='O' || arena1[lin-1][col+1]=='O') // Condi��o para evitar sobrepor os navios
		{
	     	erro ();
			mostrar_arena1 ();
			alocar_submarino1 (); // Reinicia a fun��o caso der erro
		}
		else
		{
			if (lin-1 < 0 || lin-1 > 14 || col - 1 < 0 || col - 1 > 13  || col < 0 || col > 13 || col + 1 < 0 || col + 1 > 13) // Condi��o para limitar os extremos da matriz
			{
          		erro_tabuleiro ();
				mostrar_arena1 ();
				alocar_submarino1 (); // Reinicia a fun��o caso der erro
			}
			else
			{
				arena1[lin-1][col-1]='O';
				arena1[lin-1][col]='O';
				arena1[lin-1][col+1]='O';
				mostrar_arena1 ();
			}
		}
	}

	if (sentido == 'b' || sentido == 'B')
	{
		if (arena1[lin-1][col-1]=='O' || arena1[lin][col-1]=='O' || arena1[lin+1][col-1]=='O') // Condi��o para evitar sobrepor os navios
		{
			erro ();
			mostrar_arena1 ();
			alocar_submarino1 (); // Reinicia a fun��o caso der erro
		}
		else
		{
			if (lin - 1 < 0 || lin-1 > 13 || lin < 0 || lin > 13 || lin + 1 < 0 || lin + 1 > 13 || col - 1 < 0 || col - 1 > 14 ) // Condi��o para limitar os extremos da matriz
			{
        	  	 erro_tabuleiro ();
				 mostrar_arena1 ();
				 alocar_submarino1 (); // Reinicia a fun��o caso der erro
			}
			else
			{
				arena1[lin-1][col-1]='O';
				arena1[lin][col-1]='O';
				arena1[lin+1][col-1]='O';
				mostrar_arena1 ();
			}
		}
	}

	if (sentido == 'e' || sentido == 'E')
	{
		if (arena1[lin-1][col-1]=='O' || arena1[lin-1][col-2]=='O' || arena1[lin-1][col-3]=='O') // Condi��o para evitar sobrepor os navios
		{
			erro ();
			mostrar_arena1 ();
			alocar_submarino1 (); // Reinicia a fun��o caso der erro
		}
		else
		{
			if (lin - 1 < 0 || lin-1 > 14 || col - 1 < 0 || col - 1 > 14  || col - 2 < 0 || col - 2 > 14  || col - 3 < 0 || col - 3 > 14) // Condi��o para limitar os extremos da matriz
			{
				erro_tabuleiro ();
			    mostrar_arena1 ();
			    alocar_submarino1 (); // Reinicia a fun��o caso der erro
			}
			else
			{
				arena1[lin-1][col-1]='O';
			    arena1[lin-1][col-2]='O';
			    arena1[lin-1][col-3]='O';
			    mostrar_arena1 ();
			}
		}
	}

	if (sentido == 'c' || sentido == 'C')
	{
		if (arena1[lin-1][col-1]=='O' || arena1[lin-2][col-1]=='O' || arena1[lin-3][col-1]=='O')
		{
			erro ();
			mostrar_arena1 ();
			alocar_submarino1 (); // Reinicia a fun��o caso der erro
		}
		else
		{
			if (col - 1 < 0 || col - 1 > 14  || lin - 1 < 0 || lin-1 > 14 || lin - 2 < 0 || lin - 2 > 14 || lin - 3 < 0 || lin - 3 > 14) // Condi��o para limitar os extremos da matriz
			{
				erro_tabuleiro ();
			    mostrar_arena1 ();
			    alocar_submarino1 (); // Reinicia a fun��o caso der erro
			}
			else
			{
				arena1[lin-1][col-1]='O';
			    arena1[lin-2][col-1]='O';
			    arena1[lin-3][col-1]='O';
			    mostrar_arena1();
			}
		}
	}
}

void alocar_fragata1 () // Faz a aloca��o do fragata para o Jogador 1
{
	char sentido;
	printf ("\n\n\t\tInforme a coordenada do Fragata:");
	printf ("\n\t\tLetra: ");
	scanf ("%s", &letracol);
	while (letracol != 'A' && letracol != 'B' && letracol != 'C'&& letracol != 'D' && letracol != 'E' && letracol != 'F' && letracol != 'G' && letracol != 'H' && letracol != 'I' && letracol != 'J' && letracol != 'K' && letracol != 'L' && letracol != 'M' && letracol != 'N'  && letracol != 'a' && letracol != 'b' && letracol != 'c'&& letracol != 'd' && letracol != 'e' && letracol != 'f' && letracol != 'g' && letracol != 'h' && letracol != 'i' && letracol != 'j' && letracol != 'k' && letracol != 'l' && letracol != 'm' && letracol != 'n')
	{
		erro_letra ();
		mostrar_arena1 ();
		printf ("\n\n\t\tInforme a coordenada do Corveta:");
		printf ("\n\t\tLetra: ");
		scanf ("%s", &letracol);
	}
	printf ("\t\tN%cmero: ", 163);
	scanf ("%d", &lin);
	while (lin < 1 || lin > 14)
	{
		erro_numero ();
		mostrar_arena1 ();
		printf("\n\n\t\tN%cmero: ", 163);
		scanf ("%d", &lin);
	}
	letra_numero ();
	printf ("\n\t\tSentidos dispon%cveis: \n", 213);
	printf ("\t\t[d] - Direita\n");
	printf ("\t\t[e] - Esquerda\n");
	printf ("\t\t[c] - Cima\n");
	printf ("\t\t[b] - Baixo\n");
	printf ("\n");
	printf ("\t\tInforme o sentido desejado: ");
	scanf ("%s", &sentido);
	while (sentido != 'd' && sentido != 'D' && sentido != 'b' && sentido != 'B' && sentido != 'e' && sentido != 'E' && sentido != 'c' && sentido != 'C')
	{
		erro_sentido ();
		mostrar_arena1 ();
		printf ("\n\n\t\tSentidos dispon%cveis: \n", 213);
		printf ("\t\t[d] - Direita\n");
		printf ("\t\t[e] - Esquerda\n");
		printf ("\t\t[c] - Cima\n");
		printf ("\t\t[b] - Baixo\n");
		printf ("\n");
		printf ("\t\tInforme o sentido desejado: ");
		scanf ("%s", &sentido);
	}
	if (sentido == 'd' || sentido == 'D')
	{
		if (arena1[lin-1][col-1]=='O' || arena1[lin-1][col]=='O' || arena1[lin-1][col+1]=='O') // Condi��o para evitar sobrepor os navios
		{
	     	erro ();
			mostrar_arena1 ();
			alocar_fragata1 (); // Reinicia a fun��o caso der erro
		}
		else
		{
			if (lin-1 < 0 || lin-1 > 14 || col - 1 < 0 || col - 1 > 13  || col < 0 || col > 13 || col + 1 < 0 || col + 1 > 13) // Condi��o para limitar os extremos da matriz
			{
	        	erro_tabuleiro ();
				mostrar_arena1 ();
				alocar_fragata1 (); // Reinicia a fun��o caso der erro
			}
			else
			{
				arena1[lin-1][col-1]='O';
				arena1[lin-1][col]='O';
				arena1[lin-1][col+1]='O';
				mostrar_arena1 ();
			}
		}
	}

	if (sentido == 'b' || sentido == 'B')
	{
		if (arena1[lin-1][col-1]=='O' || arena1[lin][col-1]=='O' || arena1[lin+1][col-1]=='O')
		{
			erro ();
			mostrar_arena1 ();
			alocar_fragata1 (); // Reinicia a fun��o caso der erro
		}
		else
		{
			if (lin - 1 < 0 || lin-1 > 13 || lin < 0 || lin > 13 || lin + 1 < 0 || lin + 1 > 13 || col - 1 < 0 || col - 1 > 14 ) // Condi��o para limitar os extremos da matriz
			{
          	 	erro_tabuleiro ();
			 	mostrar_arena1 ();
			 	alocar_fragata1 (); // Reinicia a fun��o caso der erro
			}
			else
			{
				arena1[lin-1][col-1]='O';
				arena1[lin][col-1]='O';
				arena1[lin+1][col-1]='O';
				mostrar_arena1 ();
			}
		}
	}

	if (sentido == 'e' || sentido == 'E')
	{
		if (arena1[lin-1][col-1]=='O' || arena1[lin-1][col-2]=='O' || arena1[lin-1][col-3]=='O') // Condi��o para evitar sobrepor os navios
		{
			erro ();
			mostrar_arena1 ();
			alocar_fragata1 (); // Reinicia a fun��o caso der erro
		}
		else
		{
			if (lin - 1 < 0 || lin-1 > 14 || col - 1 < 0 || col - 1 > 14  || col - 2 < 0 || col - 2 > 14  || col - 3 < 0 || col - 3 > 14) // Condi��o para limitar os extremos da matriz
			{
				erro_tabuleiro ();
			    mostrar_arena1 ();
			    alocar_fragata1 (); // Reinicia a fun��o caso der erro
			}
			else
			{
				arena1[lin-1][col-1]='O';
			    arena1[lin-1][col-2]='O';
			    arena1[lin-1][col-3]='O';
			    mostrar_arena1 ();
			}
		}
	}

	if (sentido == 'c' || sentido == 'C')
	{
		if (arena1[lin-1][col-1]=='O' || arena1[lin-2][col-1]=='O' || arena1[lin-3][col-1]=='O') // Condi��o para evitar sobrepor os navios
		{
			erro ();
			mostrar_arena1 ();
			alocar_fragata1 (); // Reinicia a fun��o caso der erro
		}
		else
		{
			if (col - 1 < 0 || col - 1 > 14  || lin - 1 < 0 || lin-1 > 14 || lin - 2 < 0 || lin - 2 > 14 || lin - 3 < 0 || lin - 3 > 14) // Condi��o para limitar os extremos da matriz
			{
				erro_tabuleiro ();
			    mostrar_arena1 ();
			    alocar_fragata1 (); // Reinicia a fun��o caso der erro
			}
			else
			{
				arena1[lin-1][col-1]='O';
			    arena1[lin-2][col-1]='O';
			    arena1[lin-3][col-1]='O';
			    mostrar_arena1 ();
			}
		}
	}
}

void alocar_destroyer1 () // Faz a aloca��o do destroyer para o Jogador 1
{
	char sentido;
	printf ("\n\n\t\tInforme a coordenada do Destroyer:");
	printf ("\n\t\tLetra: ");
	scanf ("%s", &letracol);
	while (letracol != 'A' && letracol != 'B' && letracol != 'C'&& letracol != 'D' && letracol != 'E' && letracol != 'F' && letracol != 'G' && letracol != 'H' && letracol != 'I' && letracol != 'J' && letracol != 'K' && letracol != 'L' && letracol != 'M' && letracol != 'N'  && letracol != 'a' && letracol != 'b' && letracol != 'c'&& letracol != 'd' && letracol != 'e' && letracol != 'f' && letracol != 'g' && letracol != 'h' && letracol != 'i' && letracol != 'j' && letracol != 'k' && letracol != 'l' && letracol != 'm' && letracol != 'n')
	{
		erro_letra ();
		mostrar_arena1 ();
		printf ("\n\n\t\tInforme a coordenada do Corveta:");
		printf ("\n\t\tLetra: ");
		scanf ("%s", &letracol);
	}
	printf ("\t\tN%cmero: ", 163);
	scanf ("%d", &lin);
	while (lin < 1 || lin > 14)
	{
		erro_numero ();
		mostrar_arena1 ();
		printf("\n\n\t\tN%cmero: ", 163);
		scanf ("%d", &lin);
	}
	letra_numero ();
	printf ("\n\t\tSentidos dispon%cveis: \n", 213);
	printf ("\t\t[d] - Direita\n");
	printf ("\t\t[e] - Esquerda\n");
	printf ("\t\t[c] - Cima\n");
	printf ("\t\t[b] - Baixo\n");
	printf ("\n");
	printf ("\t\tInforme o sentido desejado: ");
	scanf ("%s", &sentido);
	while (sentido != 'd' && sentido != 'D' && sentido != 'b' && sentido != 'B' && sentido != 'e' && sentido != 'E' && sentido != 'c' && sentido != 'C')
	{
		erro_sentido ();
		mostrar_arena1 ();
		printf ("\n\n\t\tSentidos dispon%cveis: \n", 213);
		printf ("\t\t[d] - Direita\n");
		printf ("\t\t[e] - Esquerda\n");
		printf ("\t\t[c] - Cima\n");
		printf ("\t\t[b] - Baixo\n");
		printf ("\n");
		printf ("\t\tInforme o sentido desejado: ");
		scanf ("%s", &sentido);
	}

	if (sentido == 'd' || sentido == 'D')
	{
		if (arena1[lin-1][col-1]=='O' || arena1[lin-1][col]=='O' || arena1[lin-1][col+1]=='O') // Condi��o para evitar sobrepor os navios
		{
	     	erro ();
			mostrar_arena1 ();
			alocar_destroyer1 (); // Reinicia a fun��o caso der erro
		}
		else
		{
			if (lin-1 < 0 || lin-1 > 14 || col - 1 < 0 || col - 1 > 13  || col < 0 || col > 13 || col + 1 < 0 || col + 1 > 13 || col + 2 < 0 || col + 2 > 13) // Condi��o para limitar os extremos da matriz
			{
          		erro_tabuleiro ();
				mostrar_arena1 ();
				alocar_destroyer1 (); // Reinicia a fun��o caso der erro
			}
			else
			{
				arena1[lin-1][col-1]='O';
				arena1[lin-1][col]='O';
				arena1[lin-1][col+1]='O';
				arena1[lin-1][col+2]='O';
				mostrar_arena1 ();
			}
		}
	}

	if (sentido == 'b' || sentido == 'B')
	{
		if (arena1[lin-1][col-1]=='O' || arena1[lin][col-1]=='O' || arena1[lin+1][col-1]=='O') // Condi��o para evitar sobrepor os navios
		{
			erro ();
			mostrar_arena1 ();
			alocar_destroyer1 (); // Reinicia a fun��o caso der erro
		}
		else
		{
			if(lin - 1 < 0 || lin-1 > 13 || lin < 0 || lin > 13 || lin + 1 < 0 || lin + 1 > 13 || lin + 2 < 0 || lin + 2 > 13 || col - 1 < 0 || col - 1 > 14 ) // Condi��o para limitar os extremos da matriz
			{
          		 erro_tabuleiro ();
				 mostrar_arena1 ();
				 alocar_destroyer1 (); // Reinicia a fun��o caso der erro
			}
			else
			{
				 arena1[lin-1][col-1]='O';
				 arena1[lin][col-1]='O';
				 arena1[lin+1][col-1]='O';
				 arena1[lin+2][col-1]='O';
				 mostrar_arena1 ();
			}
		}
	}

	if (sentido == 'e' || sentido == 'E')
	{
		if (arena1[lin-1][col-1]=='O' || arena1[lin-1][col-2]=='O' || arena1[lin-1][col-3]=='O') // Condi��o para evitar sobrepor os navios
		{
			erro ();
			mostrar_arena1 ();
			alocar_destroyer1 ();  // Reinicia a fun��o caso der erro
		}
		else
		{
			if (lin - 1 < 0 || lin-1 > 14 || col - 1 < 0 || col - 1 > 14  || col - 2 < 0 || col - 2 > 14  || col - 3 < 0 || col - 3 > 14 || col - 4 < 0 || col - 4 > 14) // Condi��o para limitar os extremos da matriz
			{
				erro_tabuleiro ();
			    mostrar_arena1 ();
			    alocar_destroyer1 (); // Reinicia a fun��o caso der erro
			}
			else
			{
				arena1[lin-1][col-1]='O';
			    arena1[lin-1][col-2]='O';
			    arena1[lin-1][col-3]='O';
				arena1[lin-1][col-4]='O';
			    mostrar_arena1 ();
			}
		}
	}

	if (sentido == 'c' || sentido == 'C')
	{
		if (arena1[lin-1][col-1]=='O' || arena1[lin-2][col-1]=='O' || arena1[lin-3][col-1]=='O') // Condi��o para evitar sobrepor os navios
		{
			erro ();
			mostrar_arena1 ();
			alocar_destroyer1 (); // Reinicia a fun��o caso der erro
		}
		else
		{
			if (col - 1 < 0 || col - 1 > 14  || lin - 1 < 0 || lin-1 > 14 || lin - 2 < 0 || lin - 2 > 14 || lin - 3 < 0 || lin - 3 > 14 || lin - 4 < 0 || lin - 4 > 14)
			{
				erro ();
			    mostrar_arena1 ();
			    alocar_destroyer1 (); // Reinicia a fun��o caso der erro
			}
			else
			{
				arena1[lin-1][col-1]='O';
			    arena1[lin-2][col-1]='O';
			    arena1[lin-3][col-1]='O';
				arena1[lin-4][col-1]='O';
			    mostrar_arena1 ();
			}
		}
	}
}

void alocar_cruzador1 () // Faz a aloca��o do cruzador para o Jogador 1
{
	char sentido;
	printf ("\n\n\t\tInforme a coordenada do Cruzador:");
	printf ("\n\t\tLetra: ");
	scanf ("%s", &letracol);
	while (letracol != 'A' && letracol != 'B' && letracol != 'C'&& letracol != 'D' && letracol != 'E' && letracol != 'F' && letracol != 'G' && letracol != 'H' && letracol != 'I' && letracol != 'J' && letracol != 'K' && letracol != 'L' && letracol != 'M' && letracol != 'N'  && letracol != 'a' && letracol != 'b' && letracol != 'c'&& letracol != 'd' && letracol != 'e' && letracol != 'f' && letracol != 'g' && letracol != 'h' && letracol != 'i' && letracol != 'j' && letracol != 'k' && letracol != 'l' && letracol != 'm' && letracol != 'n')
	{
		erro_letra ();
		mostrar_arena1 ();
		printf ("\n\n\t\tInforme a coordenada do Corveta:");
		printf ("\n\t\tLetra: ");
		scanf ("%s", &letracol);
	}
	printf ("\t\tN%cmero: ", 163);
	scanf ("%d", &lin);
	while (lin < 1 || lin > 14)
	{
		erro_numero ();
		mostrar_arena1();
		printf("\n\n\t\tN%cmero: ", 163);
		scanf("%d", &lin);
	}
	letra_numero();
	printf ("\n\t\tSentidos dispon%cveis: \n", 213);
	printf ("\t\t[d] - Direita\n");
	printf ("\t\t[e] - Esquerda\n");
	printf ("\t\t[c] - Cima\n");
	printf ("\t\t[b] - Baixo\n");
	printf ("\n");
	printf ("\t\tInforme o sentido desejado: ");
	scanf ("%s", &sentido);
	while (sentido != 'd' && sentido != 'D' && sentido != 'b' && sentido != 'B' && sentido != 'e' && sentido != 'E' && sentido != 'c' && sentido != 'C')
	{
		erro_sentido ();
		mostrar_arena1 ();
		printf ("\n\n\t\tSentidos dispon%cveis: \n", 213);
		printf ("\t\t[d] - Direita\n");
		printf ("\t\t[e] - Esquerda\n");
		printf ("\t\t[c] - Cima\n");
		printf ("\t\t[b] - Baixo\n");
		printf ("\n");
		printf ("\t\tInforme o sentido desejado: ");
		scanf ("%s", &sentido);
	}

	if (sentido == 'd' || sentido == 'D')
	{
		if (arena1[lin-1][col-1]=='O' || arena1[lin-1][col]=='O' || arena1[lin-1][col+1]=='O') //Condi��o para evitar sobrepor os navios
		{
	     	erro ();
			mostrar_arena1 ();
			alocar_cruzador1 (); // Reinicia a fun��o caso der erro
		}
		else
		{
			if (lin-1 < 0 || lin-1 > 14 || col - 1 < 0 || col - 1 > 13  || col < 0 || col > 13 || col + 1 < 0 || col + 1 > 13 || col + 2 < 0 || col + 2 > 13 || col + 3 < 0 || col + 3 > 13)//Checa os limites do tabuleiro
			{
        	  	erro_tabuleiro ();
				mostrar_arena1 ();
				alocar_cruzador1 (); // Reinicia a fun��o caso der erro
			}
			else
			{
				arena1[lin-1][col-1]='O';
				arena1[lin-1][col]='O';
				arena1[lin-1][col+1]='O';
				arena1[lin-1][col+2]='O';
				arena1[lin-1][col+3]='O';
				mostrar_arena1 ();
			}
		}
	}

	if (sentido == 'b' || sentido == 'B')
	{
		if (arena1[lin-1][col-1]=='O' || arena1[lin][col-1]=='O' || arena1[lin+1][col-1]=='O') // Condi��o para evitar sobrepor os navios
		{
			erro ();
			mostrar_arena1();
			alocar_cruzador1(); // Reinicia a fun��o caso der erro
		}
		else
		{
			if (lin - 1 < 0 || lin-1 > 13 || lin < 0 || lin > 13 || lin + 1 < 0 || lin + 1 > 13 || lin + 2 < 0 || lin + 2 > 13 || lin + 3 < 0 || lin + 3 > 13 || col - 1 < 0 || col - 1 > 14 )
			{
          		erro_tabuleiro ();
			 	mostrar_arena1 ();
			 	alocar_cruzador1 (); // Reinicia a fun��o caso der erro
			}
			else
			{
			 	arena1[lin-1][col-1]='O';
			 	arena1[lin][col-1]='O';
			 	arena1[lin+1][col-1]='O';
			 	arena1[lin+2][col-1]='O';
			 	arena1[lin+3][col-1]='O';
			 	mostrar_arena1 ();
			}
		}
	}

	if (sentido == 'e' || sentido == 'E')
	{
		if (arena1[lin-1][col-1]=='O' || arena1[lin-1][col-2]=='O' || arena1[lin-1][col-3]=='O') // Condi��o para evitar sobrepor os navios
		{
			erro ();
			mostrar_arena1 ();
			alocar_cruzador1 ();  // Reinicia a fun��o caso der erro
		}
		else
		{
			if (lin - 1 < 0 || lin-1 > 14 || col - 1 < 0 || col - 1 > 14  || col - 2 < 0 || col - 2 > 14  || col - 3 < 0 || col - 3 > 14 || col - 4 < 0 || col - 4 > 14 || col - 5 < 0 || col - 5 > 14)
			{
				erro_tabuleiro ();
			    mostrar_arena1 ();
			    alocar_cruzador1 (); // Reinicia a fun��o caso der erro
			}
			else
			{
				arena1[lin-1][col-1]='O';
			    arena1[lin-1][col-2]='O';
			    arena1[lin-1][col-3]='O';
				arena1[lin-1][col-4]='O';
				arena1[lin-1][col-5]='O';
			    mostrar_arena1 ();
			}
		}
	}

	if (sentido == 'c' || sentido == 'C')
	{
		if (arena1[lin-1][col-1]=='O' || arena1[lin-2][col-1]=='O' || arena1[lin-3][col-1]=='O') // Condi��o para evitar sobrepor os navios
		{
			erro ();
			mostrar_arena1 ();
			alocar_cruzador1 (); // Reinicia a fun��o caso der erro
		}
		else
		{
			if (col - 1 < 0 || col - 1 > 14  || lin - 1 < 0 || lin-1 > 14 || lin - 2 < 0 || lin - 2 > 14 || lin - 3 < 0 || lin - 3 > 14 || lin - 4 < 0 || lin - 4 > 14 || lin - 5 < 0 || lin - 5 > 14)
			{
				erro_tabuleiro ();
			    mostrar_arena1 ();
			    alocar_cruzador1 (); // Reinicia a fun��o caso der erro
			}
			else
			{
				arena1[lin-1][col-1]='O';
			    arena1[lin-2][col-1]='O';
			    arena1[lin-3][col-1]='O';
				arena1[lin-4][col-1]='O';
				arena1[lin-5][col-1]='O';
			    mostrar_arena1 ();
			}
		}
	}
}

void alocar_corveta2 () // Faz a aloca��o da corveta para o Jogador 2
{
	char sentido;
	printf ("\n\n\t\tInforme a coordenada do Corveta:");
	printf ("\n\t\tLetra: ");
	scanf ("%s", &letracol);
	while (letracol != 'A' && letracol != 'B' && letracol != 'C'&& letracol != 'D' && letracol != 'E' && letracol != 'F' && letracol != 'G' && letracol != 'H' && letracol != 'I' && letracol != 'J' && letracol != 'K' && letracol != 'L' && letracol != 'M' && letracol != 'N'  && letracol != 'a' && letracol != 'b' && letracol != 'c'&& letracol != 'd' && letracol != 'e' && letracol != 'f' && letracol != 'g' && letracol != 'h' && letracol != 'i' && letracol != 'j' && letracol != 'k' && letracol != 'l' && letracol != 'm' && letracol != 'n')
	{
		erro_letra ();
		mostrar_arena2 ();
		printf ("\n\n\t\tInforme a coordenada do Corveta:");
		printf ("\n\t\tLetra: ");
		scanf ("%s", &letracol);
	}
	printf ("\t\tN%cmero: ", 163);
	scanf ("%d", &lin);
	while (lin < 1 || lin > 14)
	{
		erro_numero ();
		mostrar_arena2 ();
		printf("\n\n\t\tN%cmero: ", 163);
		scanf ("%d", &lin);
	}
	letra_numero ();
	printf ("\n\t\tSentidos dispon%cveis: \n", 213);
	printf ("\t\t[d] - Direita\n");
	printf ("\t\t[e] - Esquerda\n");
	printf ("\t\t[c] - Cima\n");
	printf ("\t\t[b] - Baixo\n");
	printf ("\n");
	printf ("\t\tInforme o sentido desejado: ");
	scanf ("%s", &sentido);
	while (sentido != 'd' && sentido != 'D' && sentido != 'b' && sentido != 'B' && sentido != 'e' && sentido != 'E' && sentido != 'c' && sentido != 'C')
	{
		erro_sentido ();
		mostrar_arena2 ();
		printf ("\n\n\t\tSentidos dispon%cveis: \n", 213);
		printf ("\t\t[d] - Direita\n");
		printf ("\t\t[e] - Esquerda\n");
		printf ("\t\t[c] - Cima\n");
		printf ("\t\t[b] - Baixo\n");
		printf ("\n");
		printf ("\t\tInforme o sentido desejado: ");
		scanf ("%s", &sentido);
	}

	if (sentido == 'd' || sentido == 'D')
	{
		if (arena2[lin-1][col-1]=='O' || arena2[lin-1][col]=='O') // Condi��o para evitar sobrepor os navios
		{
			erro ();
			mostrar_arena2 ();
			alocar_corveta2 (); // Reinicia a fun��o caso der erro
		}
		else
		{
			if(lin - 1 < 0 || lin - 1 > 14 || col - 1 < 0 || col - 1 > 13  || col < 0 || col > 13)
			{
				erro_tabuleiro ();
			    mostrar_arena2 ();
			    alocar_corveta2 (); // Reinicia a fun��o caso der erro
			}
			else
			{
				arena2[lin-1][col-1]='O';
			    arena2[lin-1][col]='O';
			    mostrar_arena2 ();
			}
		}
	}

	if (sentido == 'b' || sentido == 'B')
	{
		if (arena2[lin-1][col-1]=='O' || arena2[lin][col-1]=='O') // Condi��o para evitar sobrepor os navios
		{
			erro ();
			mostrar_arena2 ();
			alocar_corveta2 (); // Reinicia a fun��o caso der erro
		}
		else
		{
           	if (lin - 1 < 0 || lin-1 > 13 || lin < 0 || lin > 13 || col - 1 < 0 || col - 1 > 14 )
			{
          	 	erro_tabuleiro ();
			 	mostrar_arena2 ();
			  	alocar_corveta2 (); // Reinicia a fun��o caso der erro
			}
			else
			{
				arena2[lin-1][col-1]='O';
				arena2[lin][col-1]='O';
				mostrar_arena2();
			}
		}
	}

	if (sentido == 'e' || sentido == 'E')
	{
		if (arena2[lin-1][col-1]=='O' || arena2[lin-1][col-2]=='O') // Condi��o para evitar sobrepor os navios
		{
			erro ();
			mostrar_arena2 ();
			alocar_corveta2 (); // Reinicia a fun��o caso der erro
		}
		else
		{
			if (lin - 1 < 0 || lin-1 > 14 || col - 1 < 0 || col - 1 > 14  || col - 2 < 0 || col - 2 > 14)
			{
				erro_tabuleiro ();
			    mostrar_arena2 ();
			    alocar_corveta2 (); // Reinicia a fun��o caso der erro
			}
			else
			{
				arena2[lin-1][col-1]='O';
				arena2[lin-1][col-2]='O';
				mostrar_arena2 ();
	    	}
		}
   }

	if (sentido == 'c' || sentido == 'C')
	{
		if (arena2[lin-1][col-1]=='O' || arena2[lin-2][col-1]=='O') // Condi��o para evitar sobrepor os navios
		{
			erro ();
			mostrar_arena2 ();
			alocar_corveta2 (); // Reinicia a fun��o caso der erro
		}
		else
		{
			if (col - 1 < 0 || col - 1 > 14  || lin - 1 < 0 || lin-1 > 14 || lin - 2 < 0 || lin - 2 > 14)
			{
				erro_tabuleiro ();
			    mostrar_arena2 ();
			    alocar_corveta2 (); // Reinicia a fun��o caso der erro
			}
			else
			{
			 arena2[lin-1][col-1]='O';
			 arena2[lin-2][col-1]='O';
			 mostrar_arena2 ();
			}
		}
	}
 }

void alocar_submarino2 () // Faz a aloca��o do submarino para o Jogador 2
{
	char sentido;
	printf ("\n\n\t\tInforme a coordenada do Submarino:");
	printf ("\n\t\tLetra: ");
	scanf("%s", &letracol);
	while (letracol != 'A' && letracol != 'B' && letracol != 'C'&& letracol != 'D' && letracol != 'E' && letracol != 'F' && letracol != 'G' && letracol != 'H' && letracol != 'I' && letracol != 'J' && letracol != 'K' && letracol != 'L' && letracol != 'M' && letracol != 'N'  && letracol != 'a' && letracol != 'b' && letracol != 'c'&& letracol != 'd' && letracol != 'e' && letracol != 'f' && letracol != 'g' && letracol != 'h' && letracol != 'i' && letracol != 'j' && letracol != 'k' && letracol != 'l' && letracol != 'm' && letracol != 'n')
	{
		erro_letra ();
		mostrar_arena2 ();
		printf ("\n\n\t\tInforme a coordenada do Corveta:");
		printf ("\n\t\tLetra: ");
		scanf ("%s", &letracol);
	}
	printf ("\t\tN%cmero: ", 163);
	scanf ("%d", &lin);
	while (lin < 1 || lin > 14)
	{
		erro_numero ();
		mostrar_arena2 ();
		printf("\n\n\t\tN%cmero: ", 163);
		scanf ("%d", &lin);
	}
	letra_numero ();
	printf ("\n\t\tSentidos dispon%cveis: \n", 213);
	printf ("\t\t[d] - Direita\n");
	printf ("\t\t[e] - Esquerda\n");
	printf ("\t\t[c] - Cima\n");
	printf ("\t\t[b] - Baixo\n");
	printf ("\n");
	printf ("\t\tInforme o sentido desejado: ");
	scanf ("%s", &sentido);
	while (sentido != 'd' && sentido != 'D' && sentido != 'b' && sentido != 'B' && sentido != 'e' && sentido != 'E' && sentido != 'c' && sentido != 'C')
	{
		erro_sentido ();
		mostrar_arena2 ();
		printf ("\n\n\t\tSentidos dispon%cveis: \n", 213);
		printf ("\t\t[d] - Direita\n");
		printf ("\t\t[e] - Esquerda\n");
		printf ("\t\t[c] - Cima\n");
		printf ("\t\t[b] - Baixo\n");
		printf ("\n");
		printf ("\t\tInforme o sentido desejado: ");
		scanf ("%s", &sentido);
	}

	if (sentido == 'd' || sentido == 'D')
	{
		if (arena2[lin-1][col-1]=='O' || arena2[lin-1][col]=='O' || arena2[lin-1][col+1]=='O')//Condi��o para evitar sobrepor os navios
		{
	     	erro ();
			mostrar_arena2();
			alocar_submarino2(); // Reinicia a fun��o caso der erro
		}
		else
		{
			if(lin-1 < 0 || lin-1 > 14 || col - 1 < 0 || col - 1 > 13  || col < 0 || col > 13 || col + 1 < 0 || col + 1 > 13)//Checa os limites do tabuleiro
			{
          		erro_tabuleiro ();
				mostrar_arena2 ();
				alocar_submarino2 (); // Reinicia a fun��o caso der erro
			}
			else
			{
				arena2[lin-1][col-1]='O';
				arena2[lin-1][col]='O';
				arena2[lin-1][col+1]='O';
				mostrar_arena2 ();
			}
		}
	}

	if (sentido == 'b' || sentido == 'B')
	{
		if (arena2[lin-1][col-1]=='O' || arena2[lin][col-1]=='O' || arena2[lin+1][col-1]=='O') // Condi��o para evitar sobrepor os navios
		{
			erro ();
			mostrar_arena2 ();
			alocar_submarino2 (); // Reinicia a fun��o caso der erro
		}
		else
		{
			if (lin - 1 < 0 || lin-1 > 13 || lin < 0 || lin > 13 || lin + 1 < 0 || lin + 1 > 13 || col - 1 < 0 || col - 1 > 14 )
			{
          	 	erro_tabuleiro ();
			 	mostrar_arena2 ();
			 	alocar_submarino2 (); // Reinicia a fun��o caso der erro
			}
			else
			{
			 	arena2[lin-1][col-1]='O';
			 	arena2[lin][col-1]='O';
			 	arena2[lin+1][col-1]='O';
			 	mostrar_arena2 ();
			}
		}
	}

	if (sentido == 'e' || sentido == 'E')
	{
		if (arena2[lin-1][col-1]=='O' || arena2[lin-1][col-2]=='O' || arena2[lin-1][col-3]=='O') // Condi��o para evitar sobrepor os navios
		{
			erro ();
			mostrar_arena2 ();
			alocar_submarino2 (); // Reinicia a fun��o caso der erro
		}
		else
		{
			if (lin - 1 < 0 || lin-1 > 14 || col - 1 < 0 || col - 1 > 14  || col - 2 < 0 || col - 2 > 14  || col - 3 < 0 || col - 3 > 14)
			{
				erro_tabuleiro ();
			    mostrar_arena2 ();
			    alocar_submarino2 (); // Reinicia a fun��o caso der erro
			}
			else
			{
				arena2[lin-1][col-1]='O';
			    arena2[lin-1][col-2]='O';
			    arena2[lin-1][col-3]='O';
			    mostrar_arena2 ();
			}
		}
	}

	if (sentido == 'c' || sentido == 'C')
	{
		if (arena2[lin-1][col-1]=='O' || arena2[lin-2][col-1]=='O' || arena2[lin-3][col-1]=='O') // Condi��o para evitar sobrepor os navios
		{
			erro ();
			mostrar_arena2 ();
			alocar_submarino2 (); // Reinicia a fun��o caso der erro
		}
		else
		{
			if (col - 1 < 0 || col - 1 > 14  || lin - 1 < 0 || lin-1 > 14 || lin - 2 < 0 || lin - 2 > 14 || lin - 3 < 0 || lin - 3 > 14)
			{
				erro_tabuleiro ();
			    mostrar_arena2 ();
			    alocar_submarino2 (); // Reinicia a fun��o caso der erro
			}
			else
			{
				arena2[lin-1][col-1]='O';
			    arena2[lin-2][col-1]='O';
			    arena2[lin-3][col-1]='O';
			    mostrar_arena2 ();
			}
		}
	}
}

void alocar_fragata2 () // Faz a aloca��o do fragata para o Jogador 2
{
	char sentido;
	printf ("\n\n\t\tInforme a coordenada do Fragata:");
	printf ("\n\t\tLetra: ");
	scanf ("%s", &letracol);
	while (letracol != 'A' && letracol != 'B' && letracol != 'C'&& letracol != 'D' && letracol != 'E' && letracol != 'F' && letracol != 'G' && letracol != 'H' && letracol != 'I' && letracol != 'J' && letracol != 'K' && letracol != 'L' && letracol != 'M' && letracol != 'N'  && letracol != 'a' && letracol != 'b' && letracol != 'c'&& letracol != 'd' && letracol != 'e' && letracol != 'f' && letracol != 'g' && letracol != 'h' && letracol != 'i' && letracol != 'j' && letracol != 'k' && letracol != 'l' && letracol != 'm' && letracol != 'n')
	{
		erro_letra ();
		mostrar_arena2 ();
		printf ("\n\n\t\tInforme a coordenada do Corveta:");
		printf ("\n\t\tLetra: ");
		scanf ("%s", &letracol);
	}
	printf ("\t\tN%cmero: ", 163);
	scanf ("%d", &lin);
	while (lin < 1 || lin > 14)
	{
		erro_numero ();
		mostrar_arena2 ();
		printf("\n\n\t\tN%cmero: ", 163);
		scanf ("%d", &lin);
	}
	letra_numero ();
	printf ("\n\t\tSentidos dispon%cveis: \n", 213);
	printf ("\t\t[d] - Direita\n");
	printf ("\t\t[e] - Esquerda\n");
	printf ("\t\t[c] - Cima\n");
	printf ("\t\t[b] - Baixo\n");
	printf ("\n");
	printf ("\t\tInforme o sentido desejado: ");
	scanf ("%s", &sentido);
	while (sentido != 'd' && sentido != 'D' && sentido != 'b' && sentido != 'B' && sentido != 'e' && sentido != 'E' && sentido != 'c' && sentido != 'C')
	{
		erro_sentido ();
		mostrar_arena2 ();
		printf ("\n\n\t\tSentidos dispon%cveis: \n", 213);
		printf ("\t\t[d] - Direita\n");
		printf ("\t\t[e] - Esquerda\n");
		printf ("\t\t[c] - Cima\n");
		printf ("\t\t[b] - Baixo\n");
		printf ("\n");
		printf ("\t\tInforme o sentido desejado: ");
		scanf ("%s", &sentido);
	}

	if (sentido == 'd' || sentido == 'D')
	{
		if (arena2[lin-1][col-1]=='O' || arena2[lin-1][col]=='O' || arena2[lin-1][col+1]=='O') // Condi��o para evitar sobrepor os navios
		{
	     	erro ();
			mostrar_arena2 ();
			alocar_fragata2 (); // Reinicia a fun��o caso der erro
		}
		else
		{
			if(lin-1 < 0 || lin-1 > 14 || col - 1 < 0 || col - 1 > 13  || col < 0 || col > 13 || col + 1 < 0 || col + 1 > 13)//Checa os limites do tabuleiro
			{
          		erro_tabuleiro ();
				mostrar_arena2 ();
				alocar_fragata2 (); // Reinicia a fun��o caso der erro
			}
			else
			{
				arena2[lin-1][col-1]='O';
				arena2[lin-1][col]='O';
				arena2[lin-1][col+1]='O';
				mostrar_arena2 ();
			}
		}
	}

	if (sentido == 'b' || sentido == 'B')
	{
		if (arena2[lin-1][col-1]=='O' || arena2[lin][col-1]=='O' || arena2[lin+1][col-1]=='O') // Condi��o para evitar sobrepor os navios
		{
			erro ();
			mostrar_arena2 ();
			alocar_fragata2 (); // Reinicia a fun��o caso der erro
		}
		else
		{
			if (lin - 1 < 0 || lin-1 > 13 || lin < 0 || lin > 13 || lin + 1 < 0 || lin + 1 > 13 || col - 1 < 0 || col - 1 > 14 )
			{
          	 	erro_tabuleiro ();
			 	mostrar_arena2 ();
			 	alocar_fragata2 (); // Reinicia a fun��o caso der erro
			}
			else
			{
			 	arena2[lin-1][col-1]='O';
			 	arena2[lin][col-1]='O';
			 	arena2[lin+1][col-1]='O';
			 	mostrar_arena2 ();
			}
		}
	}

	if (sentido == 'e' || sentido == 'E')
	{
		if (arena2[lin-1][col-1]=='O' || arena2[lin-1][col-2]=='O' || arena2[lin-1][col-3]=='O') // Condi��o para evitar sobrepor os navios
		{
			erro ();
			mostrar_arena2 ();
			alocar_fragata2 (); // Reinicia a fun��o caso der erro
		}
		else
		{
			if (lin - 1 < 0 || lin-1 > 14 || col - 1 < 0 || col - 1 > 14  || col - 2 < 0 || col - 2 > 14  || col - 3 < 0 || col - 3 > 14)
			{
				erro_tabuleiro ();
			    mostrar_arena2 ();
			    alocar_fragata2 (); // Reinicia a fun��o caso der erro
			}
			else
			{
				arena2[lin-1][col-1]='O';
			    arena2[lin-1][col-2]='O';
			    arena2[lin-1][col-3]='O';
			    mostrar_arena2 ();
			}
		}
	}

	if (sentido == 'c' || sentido == 'C')
	{
		if (arena2[lin-1][col-1]=='O' || arena2[lin-2][col-1]=='O' || arena2[lin-3][col-1]=='O') // Condi��o para evitar sobrepor os navios
		{
			erro ();
			mostrar_arena2 ();
			alocar_fragata2 (); // Reinicia a fun��o caso der erro
		}
		else
		{
			if (col - 1 < 0 || col - 1 > 14  || lin - 1 < 0 || lin-1 > 14 || lin - 2 < 0 || lin - 2 > 14 || lin - 3 < 0 || lin - 3 > 14)
			{
				erro_tabuleiro ();
			    mostrar_arena2 ();
			    alocar_fragata2 (); // Reinicia a fun��o caso der erro
			}
			else
			{
				arena2[lin-1][col-1]='O';
			    arena2[lin-2][col-1]='O';
			    arena2[lin-3][col-1]='O';
			    mostrar_arena2 ();
			}
		}
	}
}

void alocar_destroyer2 () // Faz a aloca��o do destroyer para o Jogador 2
{
	char sentido;
	printf ("\n\n\t\tInforme a coordenada do Destroyer:");
	printf ("\n\t\tLetra: ");
	scanf ("%s", &letracol);
	while (letracol != 'A' && letracol != 'B' && letracol != 'C'&& letracol != 'D' && letracol != 'E' && letracol != 'F' && letracol != 'G' && letracol != 'H' && letracol != 'I' && letracol != 'J' && letracol != 'K' && letracol != 'L' && letracol != 'M' && letracol != 'N'  && letracol != 'a' && letracol != 'b' && letracol != 'c'&& letracol != 'd' && letracol != 'e' && letracol != 'f' && letracol != 'g' && letracol != 'h' && letracol != 'i' && letracol != 'j' && letracol != 'k' && letracol != 'l' && letracol != 'm' && letracol != 'n')
	{
		erro_letra ();
		mostrar_arena2 ();
		printf ("\n\n\t\tInforme a coordenada do Corveta:");
		printf ("\n\t\tLetra: ");
		scanf ("%s", &letracol);
	}
	printf ("\t\tN%cmero: ", 163);
	scanf ("%d", &lin);
	while (lin < 1 || lin > 14)
	{
		erro_numero ();
		mostrar_arena2 ();
		printf("\n\n\t\tN%cmero: ", 163);
		scanf ("%d", &lin);
	}
	letra_numero ();
	printf ("\n\t\tSentidos dispon%cveis: \n", 213);
	printf ("\t\t[d] - Direita\n");
	printf ("\t\t[e] - Esquerda\n");
	printf ("\t\t[c] - Cima\n");
	printf ("\t\t[b] - Baixo\n");
	printf ("\n");
	printf ("\t\tInforme o sentido desejado: ");
	scanf("%s", &sentido);
	while (sentido != 'd' && sentido != 'D' && sentido != 'b' && sentido != 'B' && sentido != 'e' && sentido != 'E' && sentido != 'c' && sentido != 'C')
	{
		erro_sentido ();
		mostrar_arena2 ();
		printf ("\n\n\t\tSentidos dispon%cveis: \n", 213);
		printf ("\t\t[d] - Direita\n");
		printf ("\t\t[e] - Esquerda\n");
		printf ("\t\t[c] - Cima\n");
		printf ("\t\t[b] - Baixo\n");
		printf ("\n");
		printf ("\t\tInforme o sentido desejado: ");
		scanf ("%s", &sentido);
	}

	if (sentido == 'd' || sentido == 'D')
	{
		if (arena2[lin-1][col-1]=='O' || arena2[lin-1][col]=='O' || arena2[lin-1][col+1]=='O') //Condi��o para evitar sobrepor os navios
		{
	     	erro ();
			mostrar_arena2 ();
			alocar_destroyer2 (); // Reinicia a fun��o caso der erro
		}
		else
		{
			if (lin-1 < 0 || lin-1 > 14 || col - 1 < 0 || col - 1 > 13  || col < 0 || col > 13 || col + 1 < 0 || col + 1 > 13 || col + 2 < 0 || col + 2 > 13)//Checa os limites do tabuleiro
			{
          		erro_tabuleiro ();
				mostrar_arena2 ();
				alocar_destroyer2 (); // Reinicia a fun��o caso der erro
			}
			else
			{
				arena2[lin-1][col-1]='O';
				arena2[lin-1][col]='O';
				arena2[lin-1][col+1]='O';
				arena2[lin-1][col+2]='O';
				mostrar_arena2 ();
			}
		}
	}

	if (sentido == 'b' || sentido == 'B')
	{
		if (arena2[lin-1][col-1]=='O' || arena2[lin][col-1]=='O' || arena2[lin+1][col-1]=='O') // Condi��o para evitar sobrepor os navios
		{
			erro ();
			mostrar_arena2 ();
			alocar_destroyer2 (); // Reinicia a fun��o caso der erro
		}
		else
		{
			if (lin - 1 < 0 || lin-1 > 13 || lin < 0 || lin > 13 || lin + 1 < 0 || lin + 1 > 13 || lin + 2 < 0 || lin + 2 > 13 || col - 1 < 0 || col - 1 > 14 )
			{
          	 	erro_tabuleiro ();
			 	mostrar_arena2 ();
			 	alocar_destroyer2 (); // Reinicia a fun��o caso der erro
			}
			else
			{
			  	arena2[lin-1][col-1]='O';
			  	arena2[lin][col-1]='O';
			  	arena2[lin+1][col-1]='O';
			 	arena2[lin+2][col-1]='O';
			 	mostrar_arena2 ();
			}
		}
	}

	if (sentido == 'e' || sentido == 'E')
	{
		if (arena2[lin-1][col-1]=='O' || arena2[lin-1][col-2]=='O' || arena2[lin-1][col-3]=='O') // Condi��o para evitar sobrepor os navios
		{
			erro ();
			mostrar_arena2 ();
			alocar_destroyer2 ();  // Reinicia a fun��o caso der erro
		}
		else
		{
			if (lin - 1 < 0 || lin-1 > 14 || col - 1 < 0 || col - 1 > 14  || col - 2 < 0 || col - 2 > 14  || col - 3 < 0 || col - 3 > 14 || col - 4 < 0 || col - 4 > 14)
			{
				erro_tabuleiro ();
			    mostrar_arena2 ();
			    alocar_destroyer2 (); // Reinicia a fun��o caso der erro
			}
			else
			{
				arena2[lin-1][col-1]='O';
			    arena2[lin-1][col-2]='O';
			    arena2[lin-1][col-3]='O';
				arena2[lin-1][col-4]='O';
			    mostrar_arena2 ();
			}
		}
	}

	if (sentido == 'c' || sentido == 'C')
	{
		if (arena2[lin-1][col-1]=='O' || arena2[lin-2][col-1]=='O' || arena2[lin-3][col-1]=='O') // Condi��o para evitar sobrepor os navios
		{
			erro ();
			mostrar_arena2 ();
			alocar_destroyer2 (); // Reinicia a fun��o caso der erro
		}
		else
		{
			if (col - 1 < 0 || col - 1 > 14  || lin - 1 < 0 || lin-1 > 14 || lin - 2 < 0 || lin - 2 > 14 || lin - 3 < 0 || lin - 3 > 14 || lin - 4 < 0 || lin - 4 > 14)
			{
				erro_tabuleiro ();
			    mostrar_arena2 ();
			    alocar_destroyer2 (); // Reinicia a fun��o caso der erro
			}
			else
			{
				arena2[lin-1][col-1]='O';
			    arena2[lin-2][col-1]='O';
			    arena2[lin-3][col-1]='O';
				arena2[lin-4][col-1]='O';
			    mostrar_arena2 ();
			}
		}
	}
}

void alocar_cruzador2 () // Faz a aloca��o do cruzador para o Jogador 2
{
	char sentido;
	printf ("\n\n\t\tInforme a coordenada do Cruzador:");
	printf ("\n\t\tLetra: ");
	scanf ("%s", &letracol);
	while (letracol != 'A' && letracol != 'B' && letracol != 'C'&& letracol != 'D' && letracol != 'E' && letracol != 'F' && letracol != 'G' && letracol != 'H' && letracol != 'I' && letracol != 'J' && letracol != 'K' && letracol != 'L' && letracol != 'M' && letracol != 'N'  && letracol != 'a' && letracol != 'b' && letracol != 'c'&& letracol != 'd' && letracol != 'e' && letracol != 'f' && letracol != 'g' && letracol != 'h' && letracol != 'i' && letracol != 'j' && letracol != 'k' && letracol != 'l' && letracol != 'm' && letracol != 'n')
	{
		erro_letra ();
		mostrar_arena2 ();
		printf ("\n\n\t\tInforme a coordenada do Corveta:");
		printf ("\n\t\tLetra: ");
		scanf ("%s", &letracol);
	}
	printf ("\t\tN%cmero: ", 163);
	scanf ("%d", &lin);
	while (lin < 1 || lin > 14)
	{
		erro_numero ();
		mostrar_arena2 ();
		printf("\n\n\t\tN%cmero: ", 163);
		scanf ("%d", &lin);
	}
	letra_numero ();
	printf ("\n\t\tSentidos dispon%cveis: \n", 213);
	printf ("\t\t[d] - Direita\n");
	printf ("\t\t[e] - Esquerda\n");
	printf ("\t\t[c] - Cima\n");
	printf ("\t\t[b] - Baixo\n");
	printf ("\n");
	printf ("\t\tInforme o sentido desejado: ");
	scanf ("%s", &sentido);
	while (sentido != 'd' && sentido != 'D' && sentido != 'b' && sentido != 'B' && sentido != 'e' && sentido != 'E' && sentido != 'c' && sentido != 'C')
	{
		erro_sentido ();
		mostrar_arena2 ();
		printf ("\n\n\t\tSentidos dispon%cveis: \n", 213);
		printf ("\t\t[d] - Direita\n");
		printf ("\t\t[e] - Esquerda\n");
		printf ("\t\t[c] - Cima\n");
		printf ("\t\t[b] - Baixo\n");
		printf ("\n");
		printf ("\t\tInforme o sentido desejado: ");
		scanf ("%s", &sentido);
	}

	if (sentido == 'd' || sentido == 'D')
	{
		if (arena2[lin-1][col-1]=='O' || arena2[lin-1][col]=='O' || arena2[lin-1][col+1]=='O') //Condi��o para evitar sobrepor os navios
		{
	     	erro ();
			mostrar_arena2 ();
			alocar_cruzador2 (); // Reinicia a fun��o caso der erro
		}
		else
		{
			if (lin-1 < 0 || lin-1 > 14 || col - 1 < 0 || col - 1 > 13  || col < 0 || col > 13 || col + 1 < 0 || col + 1 > 13 || col + 2 < 0 || col + 2 > 13 || col + 3 < 0 || col + 3 > 13)//Checa os limites do tabuleiro
			{
          		erro_tabuleiro ();
				mostrar_arena2();
				alocar_cruzador2(); // Reinicia a fun��o caso der erro
			}
			else
			{
				arena2[lin-1][col-1]='O';
				arena2[lin-1][col]='O';
				arena2[lin-1][col+1]='O';
				arena2[lin-1][col+2]='O';
				arena2[lin-1][col+3]='O';
				mostrar_arena2 ();
			}
		}
	}

	if (sentido == 'b' || sentido == 'B')
	{
		if (arena2[lin-1][col-1]=='O' || arena2[lin][col-1]=='O' || arena2[lin+1][col-1]=='O') // Condi��o para evitar sobrepor os navios
		{
			erro ();
			mostrar_arena2 ();
			alocar_cruzador2 (); // Reinicia a fun��o caso der erro
		}
		else
		{
			if (lin - 1 < 0 || lin-1 > 13 || lin < 0 || lin > 13 || lin + 1 < 0 || lin + 1 > 13 || lin + 2 < 0 || lin + 2 > 13 || lin + 3 < 0 || lin + 3 > 13 || col - 1 < 0 || col - 1 > 14 )
			{
          	 	erro_tabuleiro ();
			 	mostrar_arena2 ();
			 	alocar_cruzador2 (); // Reinicia a fun��o caso der erro
			}
			else
			{
			 	arena2[lin-1][col-1]='O';
			 	arena2[lin][col-1]='O';
			 	arena2[lin+1][col-1]='O';
			 	arena2[lin+2][col-1]='O';
			 	arena2[lin+3][col-1]='O';
			 	mostrar_arena2 ();
			}
		}
	}

	if (sentido == 'e' || sentido == 'E')
	{
		if (arena2[lin-1][col-1]=='O' || arena2[lin-1][col-2]=='O' || arena2[lin-1][col-3]=='O') // Condi��o para evitar sobrepor os navios
		{
			erro ();
			mostrar_arena2 ();
			alocar_cruzador2 ();  // Reinicia a fun��o caso der erro
		}
		else
		{
			if (lin - 1 < 0 || lin-1 > 14 || col - 1 < 0 || col - 1 > 14  || col - 2 < 0 || col - 2 > 14  || col - 3 < 0 || col - 3 > 14 || col - 4 < 0 || col - 4 > 14 || col - 5 < 0 || col - 5 > 14)
			{
				erro_tabuleiro ();
			    mostrar_arena2 ();
			    alocar_cruzador2 (); // Reinicia a fun��o caso der erro
			}
			else
			{
				arena2[lin-1][col-1]='O';
			    arena2[lin-1][col-2]='O';
			    arena2[lin-1][col-3]='O';
				arena2[lin-1][col-4]='O';
				arena2[lin-1][col-5]='O';
			    mostrar_arena2 ();
			}
		}
	}

	if (sentido == 'c' || sentido == 'C')
	{
		if (arena2[lin-1][col-1]=='O' || arena2[lin-2][col-1]=='O' || arena2[lin-3][col-1]=='O') // Condi��o para evitar sobrepor os navios
		{
			erro ();
			mostrar_arena2 ();
			alocar_cruzador2 (); // Reinicia a fun��o caso der erro
		}
		else
		{
			if (col - 1 < 0 || col - 1 > 14  || lin - 1 < 0 || lin-1 > 14 || lin - 2 < 0 || lin - 2 > 14 || lin - 3 < 0 || lin - 3 > 14 || lin - 4 < 0 || lin - 4 > 14 || lin - 5 < 0 || lin - 5 > 14)
			{
				erro_tabuleiro ();
			    mostrar_arena2 ();
			    alocar_cruzador2 (); // Reinicia a fun��o caso der erro
			}
			else
			{
				arena2[lin-1][col-1]='O';
			    arena2[lin-2][col-1]='O';
			    arena2[lin-3][col-1]='O';
				arena2[lin-4][col-1]='O';
				arena2[lin-5][col-1]='O';
			    mostrar_arena2 ();
			}
		}
	}
}

int checar_vitoria1 (int *pontos1) // Fun��o que define o Jogador 1 como vencedor
{
	if(*pontos1==17)
	{
		system ("color 0C");
		system("cls");
		printf ("\n\n\n\n\n\n\n\n\n");
		printf ("\n              ***************************************************\n");
		printf ("\n                            A BATALHA CHEGOU AO FIM!            ");
		printf ("\n                               VIT%cRIA DE %s                   ", 224, nome_jogador1);
		printf ("\n                          DESEJA INICIAR OUTRA PARTIDA?            \n");
		printf ("\n                                   1 - SIM                        ");
		printf ("\n                                   2 - N%cO                        ", 199);
		printf ("\n\n              ***************************************************\n\n\n");
		printf ("                             INFORME A SUA OP%c%cO: ", 128, 199);
		scanf ("%d", &inicia_jogo);
		
		if (inicia_jogo != 1 && inicia_jogo != 2)
		{
			erro_opcao ();
			checar_vitoria1 (&*pontos1);
		}
				
		if (inicia_jogo == 1)
		{
			inicia_jogo = 1;
			*pontos1=0;
		}
		
		if (inicia_jogo == 2)
		{
			Creditos ();
		}
	}
}

int checar_vitoria2 (int *pontos2) // Fun��o que define o Jogador 2 como vencedor
{
	if(*pontos2==17)
	{
		system ("color 0C");
		system("cls");
		printf ("\n\n\n\n\n\n\n\n\n");
		printf ("\n              ***************************************************\n");
		printf ("\n                            A BATALHA CHEGOU AO FIM!            ");
		printf ("\n                               VIT%cRIA DE %s                   ", 224, nome_jogador2);
		printf ("\n                          DESEJA INICIAR OUTRA PARTIDA?            \n");
		printf ("\n                                   1 - SIM                        ");
		printf ("\n                                   2 - N%cO                        ", 199);
		printf ("\n\n              ***************************************************\n\n\n");
		printf ("                             INFORME A SUA OP%c%cO: ", 128, 199);
		scanf ("%d", &inicia_jogo);

		if (inicia_jogo != 1 && inicia_jogo != 2)
		{
			erro_opcao ();
			checar_vitoria2 (&*pontos2);
			*pontos2=0;
		}
		
		if (inicia_jogo == 1)
		{
			inicia_jogo = 1;
		}
		
		if (inicia_jogo == 2)
		{
			Creditos ();
		}
	}
}

int atirar1 () // Fun��o que informa a vez de atirar do Jogador 1
{
	int x;	
	static int pontos1=0;
	
	x=0;
	while (inicia_jogo != 1 && x==0)	
	{
		printf("\n\n\t\tDigite a coordenada do seu tiro: ");
		printf ("\n\t\tLetra: ");
		scanf ("%s", &letracol);
		while (letracol != 'A' && letracol != 'B' && letracol != 'C'&& letracol != 'D' && letracol != 'E' && letracol != 'F' && letracol != 'G' && letracol != 'H' && letracol != 'I' && letracol != 'J' && letracol != 'K' && letracol != 'L' && letracol != 'M' && letracol != 'N'  && letracol != 'a' && letracol != 'b' && letracol != 'c'&& letracol != 'd' && letracol != 'e' && letracol != 'f' && letracol != 'g' && letracol != 'h' && letracol != 'i' && letracol != 'j' && letracol != 'k' && letracol != 'l' && letracol != 'm' && letracol != 'n')
		{
			erro_letra ();
			mostrar_mascara2 ();
			printf ("\n\n\t\tDigite a coordenada do seu tiro");
			printf ("\n\t\tLetra: ");
			scanf ("%s", &letracol);
		}
		printf ("\t\tN%cmero: ", 163);
		scanf ("%d", &lin);
		while (lin < 1 || lin > 14)
		{
			erro_numero ();
			mostrar_mascara2 ();
			printf ("\n\n\t\tN%cmero: ", 163);
			scanf ("%d", &lin);
		}
		letra_numero ();
		if (arena2[lin-1][col-1] == 'X' || arena2[lin-1][col-1] == '*')
		{
			printf ("\n\n\t");
			printf ("     ===================================================\n\t");
			printf ("     |           VOC%c J%c ATIROU NESTE LUGAR!           |\n\t", 210, 181);
			printf ("     |             INFORME OUTRA COORDENADA            |\n\t");
			printf ("     ===================================================\n\n\n");
			printf ("\t\t");
			system ("pause");
			system ("cls");
			mostrar_mascara2 ();
		}
	
		if (arena2[lin-1][col-1] == 'O')
		{
			pontos1++;
			arena2[lin-1][col-1]='X';
			mascara2[lin-1][col-1]='X';
			system ("cls");
			mostrar_mascara2 (); // Mostra o tiro em tempo real ao jogador
			printf ("\n\n\t");
			printf ("     ===================================================\n\t");
			printf ("     |                    BELO TIRO!                   |\n\t");
			printf ("     |         VOC%c ACERTOU PARTE DE UM NAVIO!         |\n\t", 210);
			printf ("     |                 ATIRE NOVAMENTE                 |\n\t");
			printf ("     ===================================================\n\n\n");
			printf ("\t\t");
			system ("pause");
			system ("cls");
			checar_vitoria1 (&pontos1);
			mostrar_mascara2 ();
		}
	
		if (arena2[lin-1][col-1]=='~')
		{
			arena2[lin-1][col-1]='*';
			mascara2[lin-1][col-1]='*';
			system ("cls");
			mostrar_mascara2();
			printf ("\n\n\t");
			printf ("     ===================================================\n\t");
			printf ("     |           QUE PENA, VOC%c ERROU O TIRO!          |\n\t", 210);
			printf ("     |         MAIS SORTE NA PR%cXIMA TENTATIVA!        |\n\t", 224);
			printf ("     ===================================================\n\n\n");
			printf ("\t\t");
			system ("pause");
			system ("cls");
			x=1;
		}
	}
	system ("cls");
}

void atirar2 () // Fun��o que informa a vez de atirar do Jogador 2
{
	int x;	
	static int pontos2=0;
	
	x=0;
	while (inicia_jogo != 1 && x==0)
	{
		printf("\n\n\t\tDigite a coordenada do seu tiro");
		printf ("\n\t\tLetra: ");
		scanf ("%s", &letracol);
		while (letracol != 'A' && letracol != 'B' && letracol != 'C'&& letracol != 'D' && letracol != 'E' && letracol != 'F' && letracol != 'G' && letracol != 'H' && letracol != 'I' && letracol != 'J' && letracol != 'K' && letracol != 'L' && letracol != 'M' && letracol != 'N'  && letracol != 'a' && letracol != 'b' && letracol != 'c'&& letracol != 'd' && letracol != 'e' && letracol != 'f' && letracol != 'g' && letracol != 'h' && letracol != 'i' && letracol != 'j' && letracol != 'k' && letracol != 'l' && letracol != 'm' && letracol != 'n')
		{
			erro_letra ();
			mostrar_mascara1 ();
			printf ("\n\n\t\tDigite a coordenada do seu tiro");
			printf ("\n\t\tLetra: ");
			scanf ("%s", &letracol);
		}
		printf ("\t\tN%cmero: ", 163);
		scanf ("%d", &lin);
		while (lin < 1 || lin > 14)
		{
			erro_numero ();
			mostrar_mascara1 ();
			printf ("\n\n\t\tN%cmero: ", 163);
			scanf ("%d", &lin);
		}
		letra_numero ();
		if (arena1[lin-1][col-1] == 'X' || arena1[lin-1][col-1] == '*')
		{
			printf ("\n\n\t");
			printf ("     ===================================================\n\t");
			printf ("     |           VOC%c J%c ATIROU NESTE LUGAR!           |\n\t", 210, 181);
			printf ("     |             INFORME OUTRA COORDENADA            |\n\t");
			printf ("     ===================================================\n\n\n");
			printf ("\t\t");
			system ("pause");
			system ("cls");
			mostrar_mascara1 ();
		}
	
		if (arena1[lin-1][col-1] == 'O')
		{
			pontos2++;
			arena1[lin-1][col-1]='X';
			mascara1[lin-1][col-1]='X';
			system ("cls");
			mostrar_mascara1 (); // Mostra o tiro em tempo real ao jogador
			printf ("\n\n\t");
			printf ("     ===================================================\n\t");
			printf ("     |                    BELO TIRO!                   |\n\t");
			printf ("     |         VOC%c ACERTOU PARTE DE UM NAVIO!         |\n\t", 210);
			printf ("     |                 ATIRE NOVAMENTE                 |\n\t");
			printf ("     ===================================================\n\n\n");
			printf ("\t\t");
			system ("pause");
			system ("cls");
			checar_vitoria2 (&pontos2);
			mostrar_mascara1 ();
	    }
	
		if (arena1[lin-1][col-1]=='~')
		{
	    	arena1[lin-1][col-1]='*';
	    	mascara1[lin-1][col-1]='*';
	    	system ("cls");
	    	mostrar_mascara1 ();
			printf ("\n\n\t");
			printf ("     ===================================================\n\t");
			printf ("     |           QUE PENA, VOC%c ERROU O TIRO!          |\n\t", 210);
			printf ("     |         MAIS SORTE NA PR%cXIMA TENTATIVA!        |\n\t", 224);
			printf ("     ===================================================\n\n\n");
			printf ("\t\t");
			system ("pause");
			system ("cls");
			x=1;
		}
	}
	system ("cls");
}

int main () //Prograna principal
{
	int opcao;
	
	while(1)
	{
		inicia_jogo=0;
		system ("color B0")	;
		printf ("\n\n\n\n\n");
		printf ("              ");
		for (i=0; i<51; i++)
			printf ("=");
				printf ("\n              |                                                 |");
				printf ("\n              |                                                 |");
				printf ("\n              |                     O JOGO                      |");
				printf ("\n              |                  BATALHA NAVAL                  |");
				printf ("\n              |                                                 |");
				printf ("\n              |                    1 - START                    |");
				printf ("\n              |                    2 - EXIT                     |");
				printf ("\n              |                                                 |");
				printf ("\n              |                                                 |");
				printf ("\n");
		printf ("              ");
		for (i=0; i<51; i++)
			printf ("=");
		printf ("\n\n\n");
		printf ("                             INFORME A SUA OP%c%cO: ", 128, 199);
		scanf ("%d", &opcao);

		if (opcao != 1 && opcao != 2)
		{
			while (opcao != 1 && opcao != 2)
			{
				erro_opcao ();
				printf ("\n\n\n\n\n");
				printf ("              ");
				for (i=0; i<51; i++)
					printf ("=");
				printf ("\n              |                                                 |");
				printf ("\n              |                                                 |");
				printf ("\n              |                     O JOGO                      |");
				printf ("\n              |                  BATALHA NAVAL                  |");
				printf ("\n              |                                                 |");
				printf ("\n              |                    1 - START                    |");
				printf ("\n              |                    2 - EXIT                     |");
				printf ("\n              |                                                 |");
				printf ("\n              |                                                 |");
				printf ("\n");
				printf ("              ");
				for (i=0; i<51; i++)
					printf ("=");
				printf ("\n\n\n");
				printf ("                             INFORME A SUA OP%c%cO: ", 128, 199);
				scanf ("%d", &opcao);
			}
		}

			if (opcao==2)
				Creditos ();
			else
				informa_nomes ();

		system ("cls");
		printf ("\n\n\n\n\n\n\n\n\n");
		printf ("\n===============================================================================");
		printf ("\n                	  CHEGOU A HORA %s!                                    ", nome_jogador1);
		printf ("                	  HORA DE CONSTRUIR SUA ARMADA ");
		printf ("\n===============================================================================\n\n\n\n");
		printf ("\t\t");
		system ("pause");
		system ("cls");
	    inicia_arena ();
	    alocar_corveta1 ();
		alocar_submarino1 ();
		alocar_fragata1 ();
		alocar_destroyer1 ();
	    alocar_cruzador1 ();
	    printf ("\n\n===============================================================================");
	    printf ("\n               BOM TRABALHO %s! NAVIOS ALOCADOS COM SUCESSO!          ", nome_jogador1);
		printf ("\n===============================================================================\n\n\n");
		printf ("\t\t");
	    system ("pause");
		system ("cls");
		printf ("\n\n\n\n\n\n\n\n\n");
		printf ("\n===============================================================================");
		printf ("\n                	  CHEGOU A HORA %s!                                    ", nome_jogador2);
		printf ("                	  HORA DE CONSTRUIR SUA ARMADA ");
		printf ("\n===============================================================================\n\n\n\n");
		printf ("\t\t");
		system ("pause");
		system ("cls");
		mostrar_arena2 ();
		alocar_corveta2 ();
		alocar_submarino2 ();
		alocar_fragata2 ();
		alocar_destroyer2 ();
		alocar_cruzador2 ();
		printf ("\n\n===============================================================================");
	    printf ("\n               BOM TRABALHO %s! NAVIOS ALOCADOS COM SUCESSO!          ", nome_jogador2);
		printf ("\n===============================================================================\n\n\n");
		printf ("\t\t");
		system ("pause");
		system ("cls");
		printf ("\n\n\n\n\n\n\n\n\n");
		printf ("===============================================================================\n");
		printf ("                	  HORA DA BATALHA!                                        ");
		printf ("                	  BOA SORTE AOS JOGADORES!                              ");
		printf ("===============================================================================\n\n\n");
		printf ("\t\t");
		system ("pause");
		system ("cls");
		while (inicia_jogo != 1)
		{
			if (inicia_jogo != 1)
			{
				printf ("\n\n\n\n\n\n\n\n\n");
				printf ("\n===============================================================================");
		    	printf ("\n                    VEZ DO JOGADOR (A) %s, D%c SEU TIRO!         ", nome_jogador1, 210);
				printf ("\n===============================================================================\n\n\n\n");
				printf ("\t\t");
				system ("pause");
				system ("cls");
			    mostrar_mascara2 ();
			    atirar1 ();	
			}
			
			if (inicia_jogo != 1)
			{
			    printf ("\n\n\n\n\n\n\n\n\n");
			    printf ("\n===============================================================================");
		    	printf ("\n                    VEZ DO JOGADOR (A) %s, D%c SEU TIRO!         ", nome_jogador2, 210);
				printf ("\n===============================================================================\n\n\n\n");
				printf ("\t\t");
				system ("pause");
				system ("cls");
				mostrar_mascara1 ();
			    atirar2 ();
			}
		}
	}
}
